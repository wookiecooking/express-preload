module.exports = {
  port: process.env.__PORT || 3000,
  viewSupport: process.env.__VIEW_SUPPORT,
  viewDirectorySupport: process.env.__VIEWDIR_CWD,
  views: process.env.__VIEW_PATH,
  viewEngine: process.env.__VIEW_ENGINE || 'jade',
  proxy: process.env.__TRUST_PROXY,
  bodyParserJSON: process.env.__BODYPARSER_JSON,
  bodyParserUrlEncoded: process.env.__BODYPARSER_URLENCODED,
  bodyParserExtend: process.env.__BODYPARSER_EXTEND || false,
  logger: process.env.__LOGGER,
  secure: process.env.__SECURE
}