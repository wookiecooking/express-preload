var express               = require('express');
var _                     = require('lodash');
var bodyParser            = require('body-parser');
var responseTime          = require('response-time');
var helmet                = require('helmet');
var logger                = require('morgan');
var lusca                 = require('lusca');
var compress              = require('compression');
var errorHandler          = require('errorhandler');
var path                  = require('path');
var opts                  = require('./opts');
var app                   = express();

if(opts.secure){
  app.use(helmet());
}

app.use(responseTime());
app.set('port', opts.port);

if(opts.viewSupport && (opts.viewDirectorySupport || opts.views) && opts.viewEngine) {
  if(opts.viewDirectorySupport) {
    app.set('views', process.cwd(), 'views')
  }
  if(opts.views) {
    app.set('views', opts.views);
  }
  app.set('view engine', opts.viewEngine);
}

app.set('json spaces', 4);

app.use(compress());

if(opts.secure){
  app.set('etag', false);
}

if(opts.logger) {
  app.use(logger('combined'));
}

if(opts.bodyParserJSON){
  app.use(bodyParser.json());
}

if(opts.bodyParserUrlEncoded) {
  app.use(bodyParser.urlencoded({ extended: bodyParserExtend }));
}

if(opts.proxy) {
  app.enable('trust proxy');
}

if(opts.secure){
  app.use(errorHandler());
  app.use(lusca({
    csrf: true,
    xframe: 'SAMEORIGIN',
    xssProtection: true
  }));
}

if(opts.bodyParserJSON || opts.bodyParserUrlEncoded) {
  app.use(function(req, res, next){
    delete req.body._csrf;
    next();
  });
};

module.exports = app